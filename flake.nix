{
  description = "PKGNAME";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in rec {
        packages = flake-utils.lib.flattenTree {
          gitAndTools = pkgs.gitAndTools;
        };
        defaultPackage = packages.hello;
        devShell = with pkgs;
          mkShell {
            buildInputs = with pkgs; [
              elmPackages.elm
              elmPackages.elm-test
              elmPackages.elm-format
              elmPackages.elm-analyse
              elmPackages.elm-language-server
            ];
          };
      });
}
